﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace DaneFirmy
{
    public partial class Browser : Form
    {
        string tempnip,nip,regon,nazwa,wojewodztwo,powiat,gmina,kod,miasto,ulica,podatnik;

        private void button2_Click(object sender, EventArgs e)
        {//get data
            GetData();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {//nip do testow
            
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8 && ch != 46)
            {
                e.Handled = true;
            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        public Browser()
        {
            InitializeComponent();
            webBrowser1.Navigate("https://wyszukiwarkaregon.stat.gov.pl/appBIR/index.aspx");
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }
        private void Start()
        {
            tempnip = textBox1.Text;
            nip = textBox1.Text;      
           
            if (Check() )
                if(CheckNIPCorrect())
                {
                webBrowser1.Document.GetElementById("btnMenuSzukajPoIdentyfikatorach").InvokeMember("Click");
                webBrowser1.Document.GetElementById("rdbIdentyfikatorNip").InvokeMember("Click");
                webBrowser1.Document.GetElementById("txtIdentyfikatory").InnerText = nip;
                webBrowser1.Document.GetElementById("btnSzukajPoIdentyfikatorach").InvokeMember("Click");
                }
                else { MessageBox.Show("Niepoprawny NIP!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error); textBox1.Clear(); }
            else { MessageBox.Show("Niepoprawny NIP!", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error); textBox1.Clear(); }

        }
        private bool Check()
        {
            string toCut = webBrowser1.Document.Body.InnerHtml;

            if (nip.Length != 10)
                return false;
            else if (nip.Any(char.IsLetter))
                return false;

          /*  if (toCut.Contains("Nie znaleziono podmiotów."))
            {
                MessageBox.Show("NIP " + nip + " nie został odnaleziony w bazie GUS!", "UWAGA", MessageBoxButtons.OK, MessageBoxIcon.Error);
                webBrowser1.Navigate("https://wyszukiwarkaregon.stat.gov.pl/appBIR/index.aspx");
                return false;
            }*/
            else return true;
        }
        private void GetData()
        {
            string tdnTag = "ap>";
            string tdEndTag = "</TD>";
            string toCut= webBrowser1.Document.Body.OuterHtml;

           int start= toCut.IndexOf("tabelaZbiorczaAltRow");
            int end = toCut.IndexOf("<DIV id=divRaportOJednostce style");

            if (start<0)
            {
                MessageBox.Show("NIP " + nip + " nie został odnaleziony w bazie GUS!", "UWAGA", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBox1.Clear();
                return;
            }

            string result = toCut.Substring(start, end-start);            

            regon = result.Substring(result.IndexOf(";'>")+3, result.IndexOf("</A></TD>")- result.IndexOf(";'>")-3);
            richTextBox1.Text = "REGON: "+regon+Environment.NewLine;

            result = result.Remove(0, result.IndexOf("</A></TD>")+9);

            podatnik = result.Substring(result.IndexOf(tdnTag)+tdnTag.Length, result.IndexOf(tdEndTag)-tdEndTag.Length*2-tdnTag.Length);
            richTextBox1.Text += "PODATNIK: " + podatnik+Environment.NewLine;
            
            result = DeleteRecord(result, tdEndTag);

            nazwa = result.Substring(result.IndexOf("<TD>")+4,result.IndexOf(tdEndTag)-result.IndexOf("<TD>")-4);
            richTextBox1.Text += "NAZWA: " + nazwa + Environment.NewLine;

            result = DeleteRecord(result, tdEndTag);

            wojewodztwo = result.Substring(result.IndexOf(tdnTag)+tdnTag.Length,result.IndexOf(tdEndTag)-result.IndexOf(tdnTag)-tdnTag.Length);
            richTextBox1.Text += "WOJEWODZTWO: " + wojewodztwo + Environment.NewLine;

            result = DeleteRecord(result, tdEndTag);

            powiat = result.Substring(result.IndexOf(tdnTag) + tdnTag.Length, result.IndexOf(tdEndTag) - result.IndexOf(tdnTag) - tdnTag.Length);
            richTextBox1.Text += "POWIAT: " + powiat + Environment.NewLine;

            result = DeleteRecord(result, tdEndTag);

            gmina = result.Substring(result.IndexOf(tdnTag) + tdnTag.Length, result.IndexOf(tdEndTag) - result.IndexOf(tdnTag) - tdnTag.Length);
            richTextBox1.Text += "GMINA: " + gmina + Environment.NewLine;

            result = DeleteRecord(result, tdEndTag);

            kod = result.Substring(result.IndexOf(tdnTag) + tdnTag.Length, result.IndexOf(tdEndTag) - result.IndexOf(tdnTag) - tdnTag.Length);
            richTextBox1.Text += "KOD POCZTOWY: " + kod + Environment.NewLine;

            result = DeleteRecord(result, tdEndTag);

            miasto = result.Substring(result.IndexOf(tdnTag) + tdnTag.Length, result.IndexOf(tdEndTag) - result.IndexOf(tdnTag) - tdnTag.Length);
            richTextBox1.Text += "MIASTO: " + miasto + Environment.NewLine;

            result = DeleteRecord(result, tdEndTag);

            ulica = result.Substring(result.IndexOf(tdnTag) + tdnTag.Length, result.IndexOf(tdEndTag) - result.IndexOf(tdnTag) - tdnTag.Length);
            richTextBox1.Text += "ULICA: " + ulica + Environment.NewLine;

            result = DeleteRecord(result, tdEndTag);    
        }
        private string DeleteRecord(string main,string key)
        {
           return main =main.Remove(0,main.IndexOf(key)+key.Length);   
        }
        private bool CheckNIPCorrect()
        {
            List<int> numberNIP = new List<int>();
            int range = nip.Length;
            int sum=0;
            for(int i=0;i<range;i++)
            {
                numberNIP.Add(Convert.ToInt32(tempnip.Substring(0,1)));
               tempnip= tempnip.Remove(0, 1);
            }

            numberNIP[0] = numberNIP[0] * 6;
            numberNIP[1] = numberNIP[1] * 5;
            numberNIP[2] = numberNIP[2] * 7;
            numberNIP[3] = numberNIP[3] * 2;
            numberNIP[4] = numberNIP[4] * 3;
            numberNIP[5] = numberNIP[5] * 4;
            numberNIP[6] = numberNIP[6] * 5;
            numberNIP[7] = numberNIP[7] * 6;
            numberNIP[8] = numberNIP[8] * 7;

            for(int i=0;i<range-1;i++)
            {
                sum += numberNIP[i];
            }
            if (sum % 11 == numberNIP[9])
                return true;
            else return false;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Start();
        }
        
    }
}
